<?php

namespace Drupal\blog_fragments;

use Drupal\node\Entity\Node;

/**
 * Class BlogFragment.
 */
class BlogFragment {

  /**
   * Constructs a new BlogFragment object.
   */
  public function __construct() {

  }

  /**
   * Get a text fragment from a random blog.
   *
   * @return string
   *   The fragment.
   */
  public function getFragment() {
    // Title and data to return as array.
    $data['fragment'] = "No fragment found";
    $data['title'] = "Louis Nagtegaal Blogs";
    // Get nids  of all blog entities.
    $nids = \Drupal::entityQuery('node')->condition('type', 'blog')->execute();
    $data['fragment'] .= " Amount: " . count($nids);
    if (count($nids) > 0) {
      // Choose a random nid.
      $values = array_values($nids);
      $current = $values[array_rand($values)];
      $data['fragment'] .= " Current: " . $current;
      // Load the nid.
      $node = Node::load($current);
      if ($node) {
        // Get the title.
        $title = $node->title->getValue();
        // Get the body.
        $body = $node->get('body')->value;
        if (!empty($body)) {
          // Strip all HTML.
          $body = strip_tags($body);
          // Get random start (= rand(body.length - fragment-length)
          $words = explode(' ', $body);
          $body_length = count($words);
          // Minimum of 3 words, max 10.
          $fragment_length = rand(3, 10);
          $fragment_start = rand(0, ($body_length - $fragment_length));
          // Get substring.
          $fragment_array = array_slice($words, $fragment_start, $fragment_length);
          $data['fragment'] = implode(' ', $fragment_array);
          $data['title'] = $title[0]['value'];
        }
      }
    }
    return $data;
  }

}
