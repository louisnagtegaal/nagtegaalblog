<?php

namespace Drupal\blog_fragments\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FragmentBlock' block.
 *
 * @Block(
 *  id = "fragment_block",
 *  admin_label = @Translation("Fragment block"),
 * )
 */
class FragmentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $fragment = \Drupal::service('blog_fragments.fragment')->getFragment();
    $build['fragment_block']['#markup'] = $fragment['fragment'];
    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
